
const punctuation=['.', ':', '...', ',', ';', '-', '?', '!', '{', '}', '[', ']', '(', ')', '—'];

class TextAnalyzer{

    constructor(text){
        this.text=text;
    }

    parseText(){
        let parseText=this.text;

        for(let i=0; i<parseText.length; i++){
            punctuation.forEach(element => {
                if(element===parseText[i]){
                    parseText=parseText.replace(parseText[i], ' ');
                }
            });

            if(parseText[i]==="'"){
                parseText=parseText.replace(parseText[i], '');
            }
        }

        let wordsArray=parseText.split(' ');

        wordsArray = wordsArray.filter(word => word.length != ' '||'');

        return wordsArray;
    }

    countLetters(){
        let getText=this.parseText();
        let numberLetters=0;

        getText.forEach(word=>{
            numberLetters+=word.length;
        });

        return numberLetters;
    }

    countWords(){
        let getText=this.parseText();
        return getText.length;

    }

    countSentences(){
        let numberSentences=0;
        let current='';
        let prev='';
        let next='';
        let textWithoutSpace = this.text;

        for(let i=0; i<textWithoutSpace.length; i++){

            if(textWithoutSpace[i]==' '){
                textWithoutSpace=textWithoutSpace.replace(textWithoutSpace[i], '');
            }
        }

        console.log(textWithoutSpace);

        for(let i=0; i<textWithoutSpace.length; i++){
            switch (textWithoutSpace[i]){
                case '.':
                    numberSentences++
                    break;
                case '!':
                    numberSentences++
                    break;
                case '?':
                    numberSentences++
                    break;
                default:
                    break;
            }

            current=textWithoutSpace[i];
            prev=textWithoutSpace[i-1];
            next=textWithoutSpace[i+1];

            (current==='.'&& prev==='.' && next==='.') ? numberSentences-=2:numberSentences;
        }
        return numberSentences;
    }
}

function insertTextStatistics(header, numberLetters, numberWords, numberSentences){
    document.getElementById('text_statistics-head').textContent = header;
    document.getElementById('text_statistics-letters').textContent = numberLetters;
    document.getElementById('text_statistics-words').textContent = numberWords;
    document.getElementById('text_statistics-sentences').textContent = numberSentences;
}

document.getElementById("text_input-result").addEventListener('click', function(){
   let freeText = new TextAnalyzer(document.getElementById("text_input-value").value);
   let header = 'Statistics';
   let numberLetters = `Letters:${freeText.countLetters()}`;
   let numberWords = `Words:${freeText.countWords()}`;
   let numberSentences = `Sentences:${freeText.countSentences()}`;
   insertTextStatistics(header, numberLetters, numberWords, numberSentences);
});

document.getElementById("text_input-clear").addEventListener('click', function(){
    insertTextStatistics("", "", "", "");
    document.getElementById("text_input-value").value = "";
 });