import Item1 from '../../assets/items/item1.jpeg';
import Item2 from '../../assets/items/item2.jpeg';
import Item3 from '../../assets/items/item3.jpg';
import Item4 from '../../assets/items/item4.jpg';
import Item5 from '../../assets/items/item5.jpeg';
import Item6 from '../../assets/items/item6.jpeg';
import Item7 from '../../assets/items/item7.jpeg';
import { ADD_TO_CART, REMOVE_ITEM, SUB_QUANTITY ,ADD_QUANTITY, SORT_LOW_PRICE, SORT_HIGHT_PRICE, SORT_NAME} from '../actions/ActionsType';


const initState = {
    items: [
        {id:1,title:'Benito', price:110,img:Item1},
        {id:2,title:'Mida', price:80,img: Item2},
        {id:3,title:'Prego',price:120,img: Item3},
        {id:4,title:'Restime', price:260,img:Item4},
        {id:5,title:'Magnum', price:160,img: Item5},
        {id:6,title:'Hi-Tec', price:95,img: Item6},
        {id:7,title:'Benito', price:11,img:Item1},
        {id:8,title:'Mida', price:39,img: Item2},
        {id:9,title:'Prego',price:10,img: Item7},
        {id:10,title:'Restime', price:20,img:Item4},
        {id:11,title:'Magnum', price:165,img: Item5},
        {id:12,title:'Hi-Tec', price:92,img: Item6},
        {id:13,title:'Benito', price:340,img:Item1},
        {id:14,title:'Mida', price:564,img: Item2},
        {id:15,title:'Prego',price:140,img: Item7},
        {id:16,title:'Restime', price:660,img:Item4},
        {id:17,title:'Magnum', price:160,img: Item5},
        {id:18,title:'Hi-Tec', price:96,img: Item6},
        {id:19,title:'Benito', price:120,img:Item7},
        {id:20,title:'Mida', price:80,img: Item2},
        {id:21,title:'Prego',price:120,img: Item3},
        {id:22,title:'Restime', price:670,img:Item4},
        {id:23,title:'Magnum', price:140,img: Item5},
        {id:24,title:'Hi-Tec', price:780,img: Item6},
        {id:25,title:'Hi-Tec', price:30,img: Item7}
    ],
    addedItems:[],
    total: 0
}

const Reducer= (state = initState,action)=>{

if(action.type === ADD_TO_CART){
    console.log("add to cart");
    let addedItem = state.items.find(item=> item.id === action.id)
    //check if the action id exists in the addedItems
   let existed_item= state.addedItems.find(item=> action.id === item.id)
   if(existed_item)
   {
      addedItem.quantity += 1 
       return{
          ...state,
           total: state.total + addedItem.price 
            }
  }
   else{
      addedItem.quantity = 1;
      //calculating the total
      let newTotal = state.total + addedItem.price 
      
      return{
          ...state,
          addedItems: [...state.addedItems, addedItem],
          total : newTotal
      }
      
  }
}
if(action.type === REMOVE_ITEM){
  let itemToRemove= state.addedItems.find(item=> action.id === item.id)
  let new_items = state.addedItems.filter(item=> action.id !== item.id)
  
  //calculating the total
  let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity )
  console.log(itemToRemove)
  return{
      ...state,
      addedItems: new_items,
      total: newTotal
  }
}

//INSIDE CART COMPONENT
if(action.type=== ADD_QUANTITY){
  let addedItem = state.items.find(item=> item.id === action.id);
    addedItem.quantity += 1;
    let newTotal = state.total + addedItem.price;
    console.log(state);

    return{
        ...state,
        total: newTotal,
        addedItems: [...state.addedItems]
    }
}

if(action.type=== SUB_QUANTITY){  
  let addedItem = state.items.find(item=> item.id === action.id) 
  //if the qt == 0 then it should be removed
  if(addedItem.quantity === 1){
      let new_items = state.addedItems.filter(item=>item.id !== action.id)
      let newTotal = state.total - addedItem.price
      return{
          ...state,
          addedItems: new_items,
          total: newTotal
      }
  }
  else {
      addedItem.quantity -= 1
      let newTotal = state.total - addedItem.price;
      return{
          ...state,
          total: newTotal,
          addedItems: [...state.addedItems]
      }
  }
  
}

if(action.type===SORT_LOW_PRICE){
    let sortedState=state.items;
    sortedState.sort((a, b) => a.price > b.price ? 1 : -1);
    state.items=sortedState;
    return  {
        ...state,
        items:[...state.items]
    };
}

if(action.type===SORT_HIGHT_PRICE){
    let sortedState=state.items;
    sortedState.sort((a, b) => a.price > b.price ? 1 : -1);
    sortedState.reverse();
    state.items=sortedState;
    return  {
        ...state,
        items:[...state.items]
    };
}

if(action.type===SORT_NAME){
    let sortedState=state.items;
    sortedState.sort((a, b) => a.title > b.title ? 1 : -1);
    state.items=sortedState;
    return  {
        ...state,
        items:[...state.items]
    };
}
return state
}

export default Reducer;