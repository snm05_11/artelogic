import { ADD_TO_CART, REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY, SORT_LOW_PRICE, SORT_HIGHT_PRICE, SORT_NAME} from './ActionsType';

export const addToCart= (id)=>{
    return{
        type: ADD_TO_CART,
        id
    };
}

export const removeItem=(id)=>{
    return{
        type: REMOVE_ITEM,
        id
    };
}

export const subtractQuantity=(id)=>{
    return{
        type: SUB_QUANTITY,
        id
    };
}

export const addQuantity=(id)=>{
    return{
        type: ADD_QUANTITY,
        id
    };
}

export const sortLowPrice = ()=>{
    return{
        type: SORT_LOW_PRICE
    }
}


export const sortHightPrice = ()=>{
    return{
        type: SORT_HIGHT_PRICE
    }
}

export const sortName = ()=>{
    return{
        type: SORT_NAME
    }
}