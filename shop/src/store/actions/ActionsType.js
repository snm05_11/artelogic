export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const SUB_QUANTITY = 'SUB_QUANTITY';
export const ADD_QUANTITY = 'ADD_QUANTITY';
export const ADD_SHIPPING = 'ADD_SHIPPING';
export const SORT_LOW_PRICE = 'SORT_LOW_PRICE';
export const SORT_HIGHT_PRICE = 'SORT_HIGHT_PRICE';
export const SORT_NAME = 'SORT_NAME';