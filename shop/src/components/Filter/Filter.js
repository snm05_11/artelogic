import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import "./Filter.css";

class Filter extends Component {
    sortByLowPrice = () => {
    this.props.dispatch({ type: "SORT_LOW_PRICE" });
};

sortByHightPrice = () => {
    this.props.dispatch({ type: "SORT_HIGHT_PRICE" });
};

sortByName = () =>{
    this.props.dispatch({ type: "SORT_NAME" });
}

    render(){
        return(
            <nav className="filter">
                <ul>
                  <li className="filter-menu_link">Order by</li>
                  <div className="filter-menu">
                    <li><Link className="filter-menu_link" to="/" onClick={this.sortByLowPrice}>Lowest to hightest</Link></li>
                    <li><Link className="filter-menu_link" to="/" onClick={this.sortByHightPrice}>Hightest to lowest</Link></li>
                    <li><Link className="filter-menu_link" to="/" onClick={this.sortByName}>A to Z</Link></li>
                  </div>
                </ul>
          </nav>
        )
    }
}

export default connect()(Filter);