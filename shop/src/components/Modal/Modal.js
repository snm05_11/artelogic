import React, { Component } from 'react';
import Modal from 'react-awesome-modal';
import { connect } from 'react-redux';
import "./Modal.css";

let totalPrice;

class ModalWindow extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
            visible : false
        }
    }

    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    render() {
        return (
            <section className="modal">
                <input type="button" value="Submit" className="modal-btn_submit" onClick={() => this.openModal()} />
                <Modal visible={this.state.visible} width="400" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div>
                        <a className="close-window" href="/" onClick={() => this.closeModal()}>	&#935;</a>
                        <p className="total">{totalPrice}$</p>
                    </div>
                </Modal>
            </section>
        );
    }
}

const mapStateToProps = (state)=>{
    totalPrice=state.total;
    return{
        items: state.addedItems
    }
};

export default connect(mapStateToProps)(ModalWindow)