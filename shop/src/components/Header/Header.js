import React from 'react';
import Cart from '../../components/Cart/Cart';
import './Header.css';
import Filter from '../Filter/Filter';

const Header = () => {
    return(
        <div className="header">
            <Filter/>
            <Cart/> 
        </div>
    )
}

export default Header;