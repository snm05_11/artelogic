import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Main.css';
import { addToCart } from '../../store/actions/Actions';
import { bindActionCreators } from "redux";

class Main extends Component{

    handleClick = (id)=>{
        this.props.addToCart(id); 
    }

    render(){
        let itemList = this.props.items.map(item=>{
            return(
                <div className="product" key={item.id}>
                            <img className="product-img" src={item.img} alt={item.title}/>
                            <div className="product-info">
                                <p className="product-title">{item.title}</p>
                                <p className="product-price">{item.price}$</p>
                            </div>
                            <button className="btn-cart"  onClick={()=>{this.handleClick(item.id)}}>add to cart</button> 
                 </div>
            );
        });
        return(
            <div className="product-list">
                <div className="product-list_item">
                    {itemList}
                </div>
            </div>
        );
    };

}  

const mapStateToProps = (state)=>{
    return {
        items: state.items
         }
    }

const mapDispatchToProps = (dispatch)=>{
    
    return{
        dispatch,
        ...bindActionCreators({ addToCart}, dispatch) 
    }
    }

export default connect(mapStateToProps, mapDispatchToProps)(Main);