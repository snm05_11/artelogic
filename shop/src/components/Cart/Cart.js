import React, { Component } from 'react';
import cart from '../../assets/shoping.png';
import { connect } from 'react-redux';
import './Cart.css';
import ModalWindow from '../Modal/Modal';
import { bindActionCreators } from "redux";
import { removeItem, addQuantity, subtractQuantity} from '../../store/actions/Actions';


class Cart extends Component{
    handleRemove = (id)=>{
        this.props.removeItem(id);
    }
 
    handleAddQuantity = (id)=>{
        this.props.addQuantity(id);
    }

    handleSubtractQuantity = (id)=>{
        this.props.subtractQuantity(id);
    }

    render(){
        console.log(this.props.items);   
        let addedItems = this.props.items.length ?
            (  
                this.props.items.map(item=>{
                    return(
                       
                        <li className="selected-product" key={item.id}>
                            <img className="selected-product_img" src={item.img} alt={item.img}/>
                            <div>
                                <p className="selected-product_title">{item.title}</p>
                                <div className="quantity">
                                    <p onClick={()=>{this.handleSubtractQuantity(item.id)}}>-</p>
                                    <p className="selected-product_Quantity">{item.quantity}</p>
                                    <p onClick={()=>{this.handleAddQuantity(item.id)}}>+</p>
                                </div>
                                <p className="selected-product_price">Price: {item.price}$</p> 
                               
                            </div>  
                            <div>
                            <button className="waves-effect waves-light btn pink remove" onClick={()=>{this.handleRemove(item.id)}}>&#10006;</button>
                            </div>
                        </li>                        
                    )
                })
            ):
             (
                <p></p>
             )
       return(
            <div className="cart">
                <label for="toggle"><img src={cart}/></label>
                <input type="checkbox" id="toggle" class="visually-hidden"/>
                    <div className="control-me">
                        <div className="cart-list">
                        <ul className="collection">
                            {addedItems}
                        </ul>
                            <ModalWindow className="modal"/>
                        </div>
                    </div>
                </div>
       )
    }
}

const mapStateToProps = (state)=>{
    return{
        items: state.addedItems
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        dispatch,
        ...bindActionCreators({ removeItem, addQuantity, subtractQuantity}, dispatch) 
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);