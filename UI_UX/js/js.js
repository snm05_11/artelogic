
function insertContent(product, price){
    document.getElementById("product-name").textContent=product;
    document.getElementById("low-price").textContent=price;
}

document.getElementById("slider-btn_two").addEventListener('click', function(){
    insertContent("Lorem", "23$");
    document.getElementById("slider-btn_one").classList.remove("slider-selected");
    document.getElementById("slider-btn_two").classList.add("slider-selected");
    document.getElementById("slider-btn_three").classList.remove("slider-selected");
});

document.getElementById("slider-btn_three").addEventListener('click', function(){
    insertContent("Ipsum", "89$");
    document.getElementById("slider-btn_one").classList.remove("slider-selected");
    document.getElementById("slider-btn_two").classList.remove("slider-selected");
    document.getElementById("slider-btn_three").classList.add("slider-selected");
});

document.getElementById("slider-btn_one").addEventListener('click', function(){
    insertContent("Sky Planter", "18$");
    document.getElementById("slider-btn_one").classList.add("slider-selected");
    document.getElementById("slider-btn_two").classList.remove("slider-selected");
    document.getElementById("slider-btn_three").classList.remove("slider-selected");
});
