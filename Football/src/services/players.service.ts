import { Injectable } from '@angular/core';
import {IPlayer} from 'src/interfaces/player.model';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import * as env from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class PlayersService {
  private  URL = env.environment.API_URL;
  constructor(private http: HttpClient) { }

  private handleError(operation: string) {
    return (err: any) => {
        let error = `error in ${operation}() retrieving ${this.URL}`;
        if(err instanceof HttpErrorResponse) {
          alert(`status: ${err.status}, ${err.statusText}`);
        }
        return Observable.throw(error);
    }
}
  getPlayers(): Observable <IPlayer[]> {
    return this.http.get<IPlayer[]>(this.URL).pipe(
      tap(data => console.log('server data:', data)), catchError(this.handleError('getData'))
    );
  }
}
