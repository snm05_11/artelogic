import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPlayer } from 'src/interfaces/player.model';
import { ActivatedRoute } from '@angular/router';
import { PlayersService } from 'src/services/players.service';;


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit{
  id: number;
  currentPlayer: IPlayer;
  connectionError = true;
  error: any;
  errorMsg: any;

  constructor(private route: ActivatedRoute,
              private playerService: PlayersService) {
    this.id = route.snapshot.params['id'];
  }

  ngOnInit(): void {
  this.playerService.getPlayers().subscribe(
      (players: IPlayer[]) => {
        players.forEach( item => {
        if (item.id == this.id) {
          this.currentPlayer = item;
          this.connectionError = false;
        }
      });
    },
      err  => this.errorMsg.status = err
    );
  }
}
