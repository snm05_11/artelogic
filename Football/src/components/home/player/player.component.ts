import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  constructor() { }

  @Input()image: string;

  @Input()name: string;

  @Output() getInfo = new EventEmitter();

  getInfoAboutPlayer(): void {
    this.getInfo.emit();
  }

  ngOnInit(): void {
  }

}
