import { Component, OnInit } from '@angular/core';
import { PlayersService } from 'src/services/players.service';
import { IPlayer } from 'src/interfaces/player.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [PlayersService]
})
export class HomeComponent implements OnInit {

  constructor(private playerService: PlayersService,
              public router: Router) { }
  players: IPlayer[] = [];
  error: any;
  connectionError = true;
  typeError: any;
  errorMsg: any;

  moreInfo(id: number) {
    this.router.navigate(['about', id]);
  }

  ngOnInit(): void {
    this.playerService.getPlayers().subscribe(
      (players: IPlayer[]) => {
        this.players = [];
        players.forEach( item => {
        this.players.push(item);
        });
        this.connectionError = false;
      },
      err  => this.errorMsg.status = err
      );
  }
}
