import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AboutComponent } from 'src/components/about/about.component';
import { HeaderComponent } from 'src/components/about/header/header.component';

const routes: Route[] = [
  {
    path: 'about',
    component: AboutComponent,
  },
];

@NgModule({
  declarations: [AboutComponent, HeaderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AboutModule { }
