export interface IPlayer {
  id: number;
  name: string;
  photo: string;
  birth?: string;
  currentTeam?: string;
  height?: string;
  games?: number;
  goals?: number;
}
